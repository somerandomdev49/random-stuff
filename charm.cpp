/*~     monomere's charm quark editor     ~*/

/* a tiny terminal-based editor in modern C++
   that doesn't use any c++ headers, the only
   things that it requires to work are `new`,
   `delete`, `memcpy` and a pointer to the OS
   specific functions that provide IO access.
   it's made to work on all operating systems
   including hobby ones! the TUI will be done
   using ANSI escape sequences, note that the
   OS in question (or it's terminal emulator)
   has to support a well-defined ANSI subset,
   or the editor will not work (documentation
   will be in this file or in another one) */

/* name comes from charm quarks, there are
   actual subatomic particles with this name,
   have a look on Wikipedia! */

/* does not currently compile */

#define generic(T) template<typename T>

extern "C" {
#include <stddef.h> /* size_t */
#include <stdint.h> /* uint8_t, uint32_t */
#include <string.h>
}

generic(U) struct NoReference      { using T = U; };
generic(U) struct NoReference<U&>  { using T = U; };
generic(U) struct NoReference<U&&> { using T = U; };

auto min(const auto &a, const auto &b) { return a > b ? b : a; }

auto move(auto t) {
    return static_cast<typename NoReference<decltype(t)>::T&&>(t);
}

generic(T) struct List {
  T *data;
  size_t length, capacity;

  List() : data(nullptr), length(0), capacity(0) {}

  List(const List<T> &other) : data(nullptr),
                               length(other.length),
                               capacity(other.capacity) {
    if(capacity == 0) return;
    data = new T[capacity]();
    memcpy(data, other.data, sizeof(T) * length);
  }

  ~List() {
    if(data != nullptr)
      delete[] data;
  }

  T &operator[](size_t index) { return data[index]; }
  const T &operator[](size_t index) const { return data[index]; }

  void resize(size_t new_capacity) {
    capacity = new_capacity;

    if(capacity == 0) {
      data = nullptr;
      return;
    }

    T *new_data = new T[capacity]();
    memcpy(data, new_data, sizeof(T) * min(length, new_capacity));
    data = new_data;
  }

  T &add(T &&value) {
    if(length >= capacity) resize(capacity + 8);
    return data[length++] = move(value);
  }

  List<T> operator+(const List<T> &other) {
    List<T> new_list(*this);
    new_list += other;
    return new_list;
  }

  List<T> &operator+=(const List<T> &other) {
    for(const auto &item : other)
      add(item);
    return *this;
  }

  void clear(bool free = false) {
    length = 0;
    if(free && data != nullptr) {
      delete[] data;
    }
  }
};

namespace utf8 {
  using Char = uint8_t;
  using Codepoint = char32_t;

  Char *get_next_codepoint(Char *string) {
    if((*string >> 6) == 2) {
      while((*string >> 6) == 2)
        ++string;
      return string;
    }

    if((*string >> 5) == 6) return string + 1;
    if((*string >> 4) == 14) return string + 2;
    if((*string >> 3) == 30) return string + 3;

    return string + 1;
  }

  Char *get_previous_codepoint(Char *string) {
    if((*string >> 5) ==  6) string -= 1;
    if((*string >> 4) == 14) string -= 1;
    if((*string >> 3) == 30) string -= 1;

    if((*string >> 6) == 2) {
      while((*string >> 6) == 2)
        --string;
    }

    return string;
  }

  struct Iterator {
    Char *string;
    Iterator &operator++() {
      string = get_next_codepoint(string);
      return *this;
    }

    Iterator &operator--() {
      string = get_previous_codepoint(string);
      return *this;
    }

    Codepoint operator*() {
      if((string[0] >> 7) == 0) return string[0];

      Codepoint cp = string[0];

      if((string[0] >> 5) == 6) {
        cp &= 0x1F;
        cp |= string[1] & 0x3F;
      } else if((string[0] >> 4) == 14) {
        cp &= 0x0F;
        cp |= string[1] & 0x3F;
        cp |= string[2] & 0x3F;
      } else if((string[0] >> 3) == 30) {
        cp &= 0x07;
        cp |= string[1] & 0x3F;
        cp |= string[2] & 0x3F;
        cp |= string[3] & 0x3F;
      }

      return cp;
    }
  };
}

struct String {
private:
  struct LongString {
    size_t size;
    size_t capacity;
    utf8::Char *data;

    void resize(size_t new_capacity) {
      capacity = new_capacity;

      if(capacity == 0) {
        data = nullptr;
        return;
      }

      utf8::Char *new_data = new utf8::Char[capacity]();
      memcpy(data, new_data, sizeof(T) * min(size, new_capacity));
      data = new_data;
    }

    utf8::Iterator add(utf8::Codepoint value) {
      auto codepoint_size = utf8::codepoint_length(value);
      if(size + codepoint_size >= capacity) resize(capacity + 8);

      if(value < 0x80) {
        data[size - 1] = (utf8::Char)value;
        data[size - 0] = '\0';
        size += 1;
      } else if(value < 0x800) {
        data[size - 1] = (utf8::Char)(value >> 6) | 0xc0;
        data[size - 0] = (utf8::Char)(value & 0x3f) | 0x80;
        data[size + 1] = '\0';
        size += 2;
      } else if(value < 0x10000) {
        data[size - 1] = (utf8::Char)(value >> 12) | 0xe0;
        data[size - 0] = (utf8::Char)((value >> 6) & 0x3f) | 0x80;
        data[size + 1] = (utf8::Char)(value & 0x3f) | 0x80;
        data[size + 2] = '\0';
        size += 3;
      } else {
        data[size - 1] = (utf8::Char)((value >> 18) | 0xf0;
        data[size - 0] = (utf8::Char)((value >> 12) & 0x3f) | 0x80;
        data[size + 1] = (utf8::Char)((value >> 6) & 0x3f) | 0x80;
        data[size + 2] = (utf8::Char)(value & 0x3f) | 0x80;
        data[size + 3] = '\0';
        size += 4;
      }

      return { data };
    }

    LongString operator+(const List<T> &other) {
      String new_list(*this);
      new_list += other;
      return new_list;
    }

    List<T> &operator+=(const List<T> &other) {
      for(const auto &item : other)
        add(item);
      return *this;
    }

    void clear(bool free = false) {
      length() = 0;
      if(free && data != nullptr) {
        delete[] data;
      }
    }
  };

public:
  size_t length;
  union {
    LongString ls;
    Char ss[sizeof(LongString)];
  };

  utf8::Iterator begin() {
    return { data };
  }
};



generic(T) struct Range {
  T a, b;
};

struct Token {
  Range<int> 
};

struct Tokenizer {
  virtual ~Tokenizer();
  virtual void tokenize(List<Token> &tokens, const String &text) = 0;
};

struct TokenizerCpp : Tokenizer {
  void tokenize(List<Token> &tokens, const String &text) override {
    for(int c : text) {
      
    }
  }
};

struct Editor {
  struct Cursor {
    int line, col;
  } cursor;
  Tokenizer *tokenizer;

  Editor() {
    tokenizer = new TokenizerCpp();
  }

  ~Editor() {
    if(tokenizer != nullptr) {
      delete tokenizer;
    }
  }
};

struct Line {
  Editor &editor;
  List<Token> tokens;
  String text;

  void retokenize() {
    tokens.clear();
    editor.tokenizer->tokenize(tokens, text);
  }

  void put(int col, const String &new_text) {
    text.insert(col, new_text);
    retokenize();
  }

  void set(const String &new_text) {
    text = new_text;
    retokenize();
  }

  bool backspace(int col) {
    text.remove(col - 1);
    retokenize();
  }
};

struct File {
  List<Line> lines;

  bool backspace(int line, int col) {
    if(line == 0 && col == 0) return false;
    if(col == 0) {
      Line &target = lines[line - 1];
      Line &source = lines[line];
      target.set(target.text + source.source);
      lines.remove(line);
    } else {
      Line &target = lines[line];
      target.backspace(col);
    }
  }
};

int main() {

}
