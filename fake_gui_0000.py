from tkinter import *
from tkinter.ttk import Progressbar, Combobox
from time import sleep
from random import randrange, randint
from string import ascii_uppercase as aUP

def rmall(r):
    for widget in r.winfo_children():
        widget.destroy()

class PEntry(Entry):
    def __init__(self, master=None, placeholder="", color='grey', show=None, **kwargs):
        super().__init__(master, **kwargs)

        self.placeholder = placeholder
        self.placeholder_color = color
        self.default_fg_color = self['fg']
        self.default_show = show
        self.placeholder_show = self['show']

        self.bind("<FocusIn>", self.foc_in)
        self.bind("<FocusOut>", self.foc_out)

        self.put_placeholder()

    def put_placeholder(self):
        self.insert(0, self.placeholder)
        self['show'] = self.placeholder_show
        self['fg'] = self.placeholder_color

    def foc_in(self, *args):
        if self['fg'] == self.placeholder_color:
            self.delete('0', 'end')
            self['fg'] = self.default_fg_color
            self['show'] = self.default_show

    def foc_out(self, *args):
        if not self.get():
            self.put_placeholder()

            
def creds(r):
    def idenui():
        spwd = pwd.get()
        sidn = idn.get()
        sacs = acs.get()
        p = Progressbar(r,
                        orient=HORIZONTAL,
                        length=200,
                        mode="determinate",
                        takefocus=True,
                        maximum=100)
        p.pack()
        for i in range(100):
            p.step()
            r.update()
            sleep(randint(20,199) / 1000)
        
        rmall(r)
        r.geometry("400x200")
        Label(r, text="Thank you for making the world a better place!",
              pady=190).pack()
        r.update()
        sleep(5)
        rmall(r)
        r.destroy()
        exit(0)
        
    r.geometry("164x130")
    cmb = Combobox(r, values=["Class " + a for a in list(aUP)], width=7)
    num = Combobox(r, values=list(range(100)), width=7)
    idn = PEntry(r, placeholder="Identificator")
    pwd = PEntry(r, placeholder="Password", show="*")
    acs = PEntry(r, placeholder="Access Code", show="*")
    btn = Button(r, text="Continue", command=idenui, width=17)

    yp = 0
    Label(r,text="Sign Up To SKOR").place(x=26,y=yp); yp += 20
    cmb.place(x=2, y=yp)
    num.place(x=92, y=yp); yp += 20
    idn.place(y=yp); yp += 20
    pwd.place(y=yp); yp += 20
    acs.place(y=yp); yp += 20
    btn.place(y=yp); yp += 20

r = Tk()
r.title("Oxwell Corp.")
creds(r)
r.mainloop()
    
