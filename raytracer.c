#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>
#include <math.h>

#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

typedef float vec2[2];
typedef float vec3[3];
typedef float vec4[4];

struct ray {
	vec3 origin;
	vec3 direction;
};

struct hit {
	vec3 p;
	vec3 n;
	float t;
	float min, max;
};

typedef bool (*hit_function)(void *object, struct ray *ray, struct hit *hit);

struct object {
	hit_function hit;
};

struct sphere {
	struct object object;
	vec3 center;
	float radius;
};

#define REP3_(SPLT, FUN, ...) FUN(0, __VA_ARGS__) SPLT FUN(1, __VA_ARGS__) SPLT FUN(2, __VA_ARGS__)
#define REP3(FUN, ...) REP3_(;, FUN, __VA_ARGS__)
#define VEC_OP_AUN_(IDX, VEC, OP) VEC[IDX] = OP VEC[IDX]
#define VEC_OP_ABIN_(IDX, VECA, VECB, OP) VECA[IDX] = VECA[IDX] OP VECB[IDX]
#define VEC_OP_ABINC_(IDX, VECA, VECB, OP) VECA[IDX] = OP VECB[IDX]
#define VEC_OP_BIN_(IDX, VECA, VECB, OP) VECA[IDX] OP VECB[IDX]
#define VEC_OP_ABINCONST_(IDX, VEC, OP, CONST) VEC[IDX] = VEC[IDX] OP CONST
#define VEC_OP_AUNCONST_(IDX, VEC, OP, CONST) VEC[IDX] = OP CONST

void v3_add_v3(vec3 a, vec3 b) { REP3(VEC_OP_ABIN_, a, b, +); }
void v3_sub_v3(vec3 a, vec3 b) { REP3(VEC_OP_ABIN_, a, b, -); }
void v3_add_f(vec3 a, float b) { REP3(VEC_OP_ABINCONST_, a, +, b); }
void v3_sub_f(vec3 a, float b) { REP3(VEC_OP_ABINCONST_, a, -, b); }
void v3_mul_f(vec3 a, float b) { REP3(VEC_OP_ABINCONST_, a, *, b); }
void v3_div_f(vec3 a, float b) { v3_mul_f(a, 1 / b); } /* might cause prec. issues */
void v3_neg(vec3 v) { REP3(VEC_OP_AUN_, v, -); }
void v3_cpy(vec3 a, vec3 b) { REP3(VEC_OP_ABINC_, a, b, ); }
float v3_dot(vec3 a, vec3 b) { return REP3_(+, VEC_OP_BIN_, a, b, *); }
float v3_mag2(vec3 v) { return v3_dot(v, v); }
float v3_mag(vec3 v) { return sqrtf(v3_mag2(v)); }
// TODO: cross_v3(vec3 o, vec3 a, vec3 b)

void ray_at(vec3 at, struct ray *ray, float t) {
	v3_cpy(at, ray->direction);
	v3_mul_f(at, t);
	v3_add_v3(at, ray->origin);
}

bool hit_sphere(struct sphere *sphere, struct ray *ray, struct hit *hit) {
	vec3 oc; v3_cpy(oc, ray->origin);
	v3_sub_v3(oc, sphere->center);
	float a = v3_mag2(ray->direction);
	float b2 = v3_dot(oc, ray->direction);
	float c = v3_mag2(oc) - sphere->radius * sphere->radius;
	float d = b2 * b2 - a * c;
	if(d < 0) return false;
	float q = sqrtf(d);
	float r = (-b2 - q) / a; /* min. root */
	if(r < hit->min || r > hit->max) {
		r = (-b2 + q) / a; /* max. root */
		if(r < hit->min || r > hit->max)
			return false;
	}
	hit->t = r;
	ray_at(hit->p, ray, hit->t);
	v3_cpy(hit->n, hit->p);
	v3_sub_v3(hit->n, sphere->center);
	v3_div_f(hit->n, sphere->radius);
	return true;
}

void ray_compute_color(vec3 color, struct ray *ray, struct object *object) {
	struct hit hit = {0};
	hit.min = 0;
	hit.max = INFINITY;
	if(object->hit(object, ray, &hit)) {
		v3_cpy(color, hit.n);
		v3_add_f(color, 1.0f);
		v3_mul_f(color, 0.5f);
		return;
	}
	static vec3 ZEROS = { 0.0f, 0.0f, 0.0f };
	v3_cpy(color, ZEROS);
	return;
}

uint32_t get_u32color(vec3 color) {
	return
		(uint8_t)(color[0] * 255) << 16 |
		(uint8_t)(color[1] * 255) <<  8 |
		(uint8_t)(color[2] * 255) <<  0 ;
}

void render(uint32_t *fb, int offset, int w, int h, int pitch) {
	float aspect = (float)w / (float)h;
	float viewh = 2.0f;
	float vieww = viewh * aspect;
	float focal = 1.0f;
	vec3 origin = { 0.0f, 0.0f, 0.0f };
	vec3 horiz = { vieww, 0.0f, 0.0f };
	vec3 vert = { 0.0f, viewh, 0.0f };
	vec3 lower_left = {
		origin[0] - vieww / 2.0f,
		origin[1] - viewh / 2.0f,
		origin[2] - focal
	};

	struct sphere sphere; {
		vec3 sphere_center = { 0.0f, 0.0f, -1.0f };
		float sphere_radius = 0.5f;
		v3_cpy(sphere.center, sphere_center);
		sphere.radius = sphere_radius;
		sphere.object.hit = (hit_function)&hit_sphere;
	}

	for(int j = 0; j < h; ++j) {
		printf("%.2f%%\n", (float)j / (float)h * 100.0f);
		for(int i = 0; i < w; ++i) {
			float u = (float)i / (float)(w - 1);
			float v = (float)j / (float)(h - 1);
			struct ray ray;
			v3_cpy(ray.origin, origin);
			v3_cpy(ray.direction, lower_left);
			ray.direction[0] += u * vieww;
			ray.direction[1] += v * viewh;
			vec3 color;
			ray_compute_color(color, &ray, &sphere.object);
			uint32_t color_u32 = get_u32color(color);
			int i2 = i * 2, j2 = j * 2;
			fb[offset + i2   + j2     * pitch] = color_u32;
			fb[offset + i2+1 + j2     * pitch] = color_u32;
			fb[offset + i2   + (j2+1) * pitch] = color_u32;
			fb[offset + i2+1 + (j2+1) * pitch] = color_u32;
		}
	}
}

int main(int argc, char *argv[]) {
	fputs("\033[2J\033[H", stdout);
	int fbfd = open("/dev/fb0", O_RDWR);
	if(!fbfd) return 1;
	struct fb_var_screeninfo vinfo;
	ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo);
	int fb_width = vinfo.xres;
	int fb_height = vinfo.yres;
	int fb_bpp = vinfo.bits_per_pixel;
	int fb_bytes = fb_bpp / 8;
	int fb_data_size = fb_width * fb_height * fb_bytes;
	char *fbdata = mmap(0, fb_data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, (off_t)0);
	memset(fbdata, 0, fb_data_size);
	uint32_t *fb = (uint32_t*)fbdata;
	// render(fb, 400, fb_width - 400, fb_height, fb_width);
	render(fb, 400 + 400 * fb_width, 128, 128, fb_width);
	munmap(fbdata, fb_data_size);
	fputs("\033[999H", stdout);
	close(fbfd);
	return 0;
}
